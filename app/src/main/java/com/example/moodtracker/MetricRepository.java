package com.example.moodtracker;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.example.moodtracker.database.MeasureDao;
import com.example.moodtracker.database.MeasureEntity;
import com.example.moodtracker.database.MeasureWithNameEntity;
import com.example.moodtracker.database.MetricNameDao;
import com.example.moodtracker.database.MetricNameEntity;
import com.example.moodtracker.database.MyDatabase;

import java.util.Date;
import java.util.List;

public class MetricRepository {

    private MeasureDao mMeasureDao;
    private MetricNameDao mMetricNameDao;
    private LiveData<List<MeasureEntity>> mAllMeasures;
    private LiveData<List<MetricNameEntity>> mAllMetricNames;
    private LiveData<Integer> mNumberOfMetricNames;
    private LiveData<List<MeasureWithNameEntity>> mMeasureWithNameEntityLiveData;

    //only for testing
    private List<MeasureEntity> mAllMeasuresList;
    private List<MetricNameEntity> mAllMetricsList;

    MetricRepository(Application application) {
        MyDatabase myDatabase = MyDatabase.getInstance(application);

        //reference to the Dao for the MeasureEntitiy
        mMeasureDao = myDatabase.mMeasureDao();

        //reference to the Dao for the MetricNameEntitiy
        mMetricNameDao = myDatabase.mMetricNameDao();

        //list of all measure entries
        mAllMeasures = mMeasureDao.getAllMeasureEntries();

        //list of metric names
        mAllMetricNames = mMetricNameDao.getAllMetricNames();

        //number of Metric Names
        mNumberOfMetricNames = mMetricNameDao.getNumOfMetricNames();

        //list of all measures with names of the metrics (JOIN)
        mMeasureWithNameEntityLiveData = mMeasureDao.getAllMeasuresWithNames();


        //only for testing
        mAllMeasuresList = mMeasureDao.getAllMeasureEntriesList();
        mAllMetricsList = mMetricNameDao.getAllMetricsList();


    }

    public LiveData<List<MeasureEntity>> getAllMeasures() {
        return mAllMeasures;
    }

    public LiveData<List<MetricNameEntity>> getAllMetricNames() {
        return mAllMetricNames;
    }

    public LiveData<Integer> getNumberOfMetricNames() {
        return mNumberOfMetricNames;
    }

    public LiveData<List<MeasureWithNameEntity>> getMeasuresForThisDate(Date date) {
        return mMeasureDao.getAllMeasuresForThisDate(date);
    }


    public void insertMeasure(MeasureEntity measureEntity) {

        new insertAsyncTask(mMeasureDao).execute(measureEntity);
        Log.d("Repository", "After the AsyncTask!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1");

    }

    public void insertMetricName(MetricNameEntity metricNameEntity) {
        mMetricNameDao.insert(metricNameEntity);
    }

    public int findIdForMetricName(String fromUserMetricName) {
        MetricNameEntity metricNameInDatabase = mMetricNameDao.findMetricIdByName(fromUserMetricName);
        if (metricNameInDatabase != null) {
            return metricNameInDatabase.getId();
        } else {
            int metricID = (int) mMetricNameDao.insert(new MetricNameEntity(fromUserMetricName));
            return metricID;
        }
    }

    public void deleteMetricByName(String metric) {
        mMetricNameDao.deleteMetricByName(metric);
    }

    public void deleteAllMeasures() {
        new deleteAllAsyncTask(mMeasureDao).execute();
    }

    public LiveData<List<MeasureWithNameEntity>> getAllMeasuresWithNames() {
        return mMeasureWithNameEntityLiveData;
    }

    public  List<MeasureWithNameEntity> getMeasuresForMetricWithName(String metric) {
        return mMeasureDao.getAllMeasuresForThisMetric(metric);
    }

    //only for testing
    public List<MeasureEntity> getAllMeasuresList() {
        return mAllMeasuresList;
    }

    public List<MetricNameEntity> getAllMetricsList() {
        return mAllMetricsList;
    }



/*
    async tasks
*/


    //async task for inserting new measure
    private static class insertAsyncTask extends AsyncTask<MeasureEntity, Void, Void> {

        private MeasureDao mMyAsyncTaskDao;

        insertAsyncTask(MeasureDao dao) {
            mMyAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final MeasureEntity... params) {
            mMyAsyncTaskDao.insert(params[0]);
            return null;
        }
    }


    //async task for deleting all measures
    private static class deleteAllAsyncTask extends AsyncTask<Void, Void, Void> {

        private MeasureDao mMyAsyncTaskDao;

        deleteAllAsyncTask(MeasureDao dao) {
            mMyAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mMyAsyncTaskDao.deleteAll();
            return null;
        }


    }

}
