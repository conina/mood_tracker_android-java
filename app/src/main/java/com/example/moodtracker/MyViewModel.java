package com.example.moodtracker;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import com.example.moodtracker.database.MeasureEntity;
import com.example.moodtracker.database.MeasureWithNameEntity;
import com.example.moodtracker.database.MetricNameEntity;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MyViewModel extends AndroidViewModel {

    private MetricRepository mRepository;
    private LiveData<List<MeasureEntity>> mAllMeasuresEntries;
    private LiveData<List<MetricNameEntity>> mAllMetricNames;
    private LiveData<Integer> mNumOfMetricNames;
    private LiveData<List<MeasureWithNameEntity>> mMeasureWithNameEntityLiveData;

    //only for testing to get the list out of the database
    private List<MeasureEntity> mAllMeasuresList;
    private List<MetricNameEntity> mAllMetricsList;

    private MutableLiveData<Date> pickedDate;


    public MyViewModel(Application application) {
        super(application);
        mRepository = new MetricRepository(application);
        mAllMeasuresEntries = mRepository.getAllMeasures();
        mAllMetricNames = mRepository.getAllMetricNames();
        mNumOfMetricNames = mRepository.getNumberOfMetricNames();

        //list of all measures with names of the metrics (JOIN)
        mMeasureWithNameEntityLiveData =  mRepository.getAllMeasuresWithNames();

        //picked date
        pickedDate = new MutableLiveData<>();

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        pickedDate.setValue(calendar.getTime());

        //only for testing
        mAllMeasuresList = mRepository.getAllMeasuresList();
        mAllMetricsList = mRepository.getAllMetricsList();
    }


    public LiveData<Date> getPickedDate() {
        return this.pickedDate;
    }

    public void setPickedDate(Date pickedDate) {
        this.pickedDate.setValue(pickedDate);
    }


    public LiveData<List<MeasureWithNameEntity>> getMeasuresForThisDate(Date date) {
        return mRepository.getMeasuresForThisDate(date);
    }
    public LiveData<List<MeasureEntity>> getAllMeasuresEntries() {
        return mAllMeasuresEntries;
    }

    public LiveData<List<MetricNameEntity>> getAllMetricNames() {
        return mAllMetricNames;
    }

    public LiveData<Integer> getNumOfMetricNames() {
        return mNumOfMetricNames;
    }
    //to insertMeasure a new measure entity, we need to check if there is an ID for that metric in the metric table.
    // if it exists, we'll use that id, if not, we'll first create an id for that metric and than create a MeasureEntity object
    public void insertMeasureIntoDatabase(String metricName, Date date, int assessment) {

        //let repository first find the id for this metric name or create a new one
        int metricId = mRepository.findIdForMetricName(metricName);

        //insert new measure into the measures_table
        mRepository.insertMeasure(new MeasureEntity(metricId, date, assessment));

        //for testing
        Log.d("Id of the Metric found", Integer.toString(metricId));
    }

    public void insertMetricNameIntoDatabase(MetricNameEntity metricNameEntity) {
        mRepository.insertMetricName(metricNameEntity);
    }

    public void deleteAll() {
        mRepository.deleteAllMeasures();
    }


    public LiveData<List<MeasureWithNameEntity>> getAllMeasuresWithNames() {
        return mMeasureWithNameEntityLiveData;
    }

    public  List<MeasureWithNameEntity> getMeasuresForMetricWithName(String metric) {
        return mRepository.getMeasuresForMetricWithName(metric);
    }

    public void deleteMetricByName(String metric) {
        mRepository.deleteMetricByName(metric);
    }

    //only for testing
    public List<MeasureEntity> getAllMeasuresList() {
        return mAllMeasuresList;
    }

    public List<MetricNameEntity> getAllMetricsList() {
        return mAllMetricsList;
    }
}
