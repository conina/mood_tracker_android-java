package com.example.moodtracker;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.moodtracker.Utils.ZoomOutTransformation;
import com.example.moodtracker.database.MetricNameEntity;

import java.util.List;

public class ViewPagerFragment extends Fragment {


    private static int numFragments;
    MyViewModel mMyViewModel;
    ViewPagerFragment.MyAdapter mMyAdapter;
    ViewPager mViewPager;

    private OnViewPagerFragmentInteractionListener mListener;

    public ViewPagerFragment() {
        // Required empty public constructor
    }

    public static ViewPagerFragment newInstance() {
        ViewPagerFragment fragment = new ViewPagerFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            //maybe add something later or delete if not needed
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragemnt_view_pager, container, false);

        //set up the viewpager and connecting it with the adapter
        mMyAdapter = new ViewPagerFragment.MyAdapter(getActivity().getSupportFragmentManager(), numFragments);
        mViewPager = (ViewPager) view;
        mViewPager.setAdapter(mMyAdapter);
        mViewPager.setPageTransformer(true, new ZoomOutTransformation());
        //create an instance of MyViewModel
        mMyViewModel = ViewModelProviders.of(this).get(MyViewModel.class);

        //observe the live data and then update the UI when a new Metric is entered
        mMyViewModel.getAllMetricNames().observe(this, new Observer<List<MetricNameEntity>>() {
            @Override
            public void onChanged(List<MetricNameEntity> metricNameEntities) {
                //setting the list of names so that Fragment can pull the name for the UI
                TemplateFragment.sListOfMetricNames = metricNameEntities;
                //setting the number of fragments so that adapter can create new fragment for the added metric
                mMyAdapter.setNumFragments(metricNameEntities);


            }
        });
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onViewPagerFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnViewPagerFragmentInteractionListener) {
            mListener = (OnViewPagerFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnViewPagerFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnViewPagerFragmentInteractionListener {
        // TODO: Update argument type and name
        void onViewPagerFragmentInteraction(Uri uri);
    }

    //adapter for the ViewPager
    public static class MyAdapter extends FragmentStatePagerAdapter {

        private int mNumFragments;
        public MyAdapter(FragmentManager fragmentManager, int numFragemnets) {
            super(fragmentManager);
            mNumFragments = numFragemnets;
        }

        @Override
        public Fragment getItem(int position) {

            return TemplateFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            Log.d("InAdapterNumberOfItems", Integer.toString(mNumFragments));
            return mNumFragments;
        }

        //this is called from MainActivity when a new metric is added
        public void setNumFragments(List<MetricNameEntity> metricNameEntities) {
            mNumFragments = metricNameEntities.size();
            Log.d("InAdaptersetNumFrag", Integer.toString(mNumFragments));
            notifyDataSetChanged();
        }
        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }
}
