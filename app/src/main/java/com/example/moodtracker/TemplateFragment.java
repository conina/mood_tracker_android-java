package com.example.moodtracker;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.preference.PreferenceManager;

import com.example.moodtracker.database.MetricNameEntity;
import com.google.android.material.button.MaterialButton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TemplateFragment extends Fragment {

    private static final String METRIC_MOOD = "Mood";
    private static final String METRIC_SEX = "Sex";
    private static final String METRIC_PROGRAMING = "Programming";
    private static final String METRIC_EXERCISE = "Exercise";
    private static final String METRIC_FOOD = "Food";

/*
    List<String> mListOfMetrics;
*/

    private RadioGroup rGroup;
    private MaterialButton confirmButton;
    public int radio_button_clicked = R.integer.nothing_selected;
    int metricNumber;
    String metricName;
    private AddMeasureListener mAddMeasureListener;

    private MyViewModel mMyViewModel;
    private View view;
    static List<MetricNameEntity> sListOfMetricNames = new ArrayList<>();

    private TextView mDateTextView;

    public static TemplateFragment newInstance(int metric_number) {
        TemplateFragment templateFragment = new TemplateFragment();

        Bundle args = new Bundle();
        args.putInt("metric_number", metric_number);
        templateFragment.setArguments(args);
        return templateFragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAddMeasureListener = (AddMeasureListener) getContext();

        //here we can get the metric number which we set when we created the fragment from adapter
        metricNumber = getArguments() != null ? getArguments().getInt("metric_number") : 1;

        // getting the instance of the viewmodel
        mMyViewModel = ViewModelProviders.of(getActivity()).get(MyViewModel.class);

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //if it's the last metric, get a different layout
        if (metricNumber == sListOfMetricNames.size() - 1) {
            view = inflater.inflate(R.layout.template_fragment_last, container, false);
            addListenerOnConfirmButton();

        } else {
            // Inflate the layout for this fragment
            view = inflater.inflate(R.layout.template_fragment, container, false);
        }
        //setting up the question for the fragment
       /* List<MetricNameEntity> metricNamesFromDB = mMyViewModel.getAllMetricsList();

        if (metricNumber < metricNamesFromDB.size() )
            metricName = metricNamesFromDB.get(metricNumber).getMetricName();
        TextView question = view.findViewById(R.id.question);
        question.setText(metricName);*/

        if (metricNumber < sListOfMetricNames.size())
            metricName = sListOfMetricNames.get(metricNumber).getMetricName();

        TextView question = view.findViewById(R.id.question);
        question.setText(metricName);

        mDateTextView = view.findViewById(R.id.date_for_template_frag);

        mMyViewModel.getPickedDate().observe(this, new Observer<Date>() {
            @Override
            public void onChanged(Date date) {

                Long pickedDateLong = date.getTime();
                DateFormat dateFormat1 = new SimpleDateFormat("MMMM dd, YYYY");
                String stringDate = dateFormat1.format(pickedDateLong);
                mDateTextView.setText(stringDate);
            }
        });





        //adding listeners for buttons
        addListenerOnRadioButton();

        return view;
    }


    public void addListenerOnRadioButton() {

        rGroup = view.findViewById(R.id.radio_group);
        rGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {

                RadioButton checkedButton = rGroup.findViewById(checkedId);

                if (checkedButton != null) {
                    switch (checkedId) {
                        case R.id.button_one:
                            radio_button_clicked = getContext().getResources().getInteger(R.integer.one);

                            Log.d("assessment number:", Integer.toString(radio_button_clicked) );
                            break;
                        case R.id.button_two:
                            radio_button_clicked = getContext().getResources().getInteger(R.integer.two);
                            Log.d("assessment number:", Integer.toString(radio_button_clicked) );
                            break;
                        case R.id.button_three:
                            radio_button_clicked = getContext().getResources().getInteger(R.integer.three);
                            Log.d("assessment number:", Integer.toString(radio_button_clicked) );


                            break;
                        case R.id.button_four:
                            radio_button_clicked = getContext().getResources().getInteger(R.integer.four);
                            Log.d("assessment number:", Integer.toString(radio_button_clicked) );

                            break;
                        case R.id.button_five:
                            radio_button_clicked = getContext().getResources().getInteger(R.integer.five);
                            Log.d("assessment number:", Integer.toString(radio_button_clicked) );

                            break;
                    }
                    mAddMeasureListener.put_to_temp_map(metricName, radio_button_clicked);

                }
            }
        });
    }

    public void addListenerOnConfirmButton() {
        confirmButton = view.findViewById(R.id.confirm_button);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (radio_button_clicked != R.integer.nothing_selected) {

//                    mAddMeasureListener = (AddMeasureListener) getContext();
                    mAddMeasureListener.addMeasuresToDatabase();

                    //setting the radio_button to empty string after it was sent off to the database
                    // so it prevents from sending the same data when clicking multiple times
                    radio_button_clicked = R.integer.nothing_selected;


                }
            }
        });
    }

    public interface AddMeasureListener {
        void put_to_temp_map(String metricName, int radioButton);
        void addMeasuresToDatabase();
    }
}