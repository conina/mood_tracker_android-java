package com.example.moodtracker;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.moodtracker.ItemFragment.OnListFragmentInteractionListener;
import com.example.moodtracker.database.MetricNameEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a  and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {

    private final List<MetricNameEntity> mValues;
    private final OnListFragmentInteractionListener mListener;

    Context mContext;

    public MyItemRecyclerViewAdapter(List<MetricNameEntity> items, OnListFragmentInteractionListener listener) {
        //exclude the How do you feel? item so it doesn't show on the list
        mValues = items.subList(1, items.size());
        Log.d("number of names", Integer.toString(mValues.size()));
        mListener = listener;
        mContext = (Context) listener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Log.d("position", Integer.toString(position));

        holder.mMetricItem = mValues.get(position);
        holder.mMetricId.setText(Integer.toString(position + 1));
        holder.mMetricName.setText(mValues.get(position).getMetricName());

        Log.d("name", mValues.get(position).getMetricName());

        holder.mOptionsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu;
                popupMenu = new PopupMenu(mContext, holder.mOptionsMenu);
                popupMenu.inflate(R.menu.menu_recyler_view_item);

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.itemDelete:

                                mValues.remove(position);
                                mListener.onListFragmentInteraction(holder.mMetricItem);
//                                notifyDataSetChanged();
                                notifyItemRemoved(holder.getAdapterPosition());
                                return true;
                        }
                        return false;
                    }
                });

                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    // ViewHolder
    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mMetricId;
        public final TextView mMetricName;
        public TextView mOptionsMenu;
        public MetricNameEntity mMetricItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mMetricId = (TextView) view.findViewById(R.id.metric_id);
            mMetricName = (TextView) view.findViewById(R.id.metric_name);
            mOptionsMenu = view.findViewById(R.id.buttonOptions);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mMetricName.getText() + "'";
        }
    }
}
