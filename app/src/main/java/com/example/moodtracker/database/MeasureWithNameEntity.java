package com.example.moodtracker.database;

import androidx.room.ColumnInfo;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Date;

public class MeasureWithNameEntity implements Comparable<MeasureWithNameEntity>{

@Ignore
    private int mId;

    public int mMetricId;

    public String mMetricName;

    public Date mDate;

    public int mAssessment;

    public MeasureWithNameEntity(int metricId, String metricName, Date date, int assessment) {
        this.mMetricId = metricId;
        this.mMetricName = metricName;
        this.mDate = date;
        this.mAssessment = assessment;
    }



    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getMetricId() {
        return mMetricId;
    }

    public void setMetricId(int metricId) {
        mMetricId = metricId;
    }

    public String getMetricName() {
        return mMetricName;
    }

    public void setMetricName(String metricName) {
        mMetricName = metricName;
    }
    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public int getAssessment() {
        return mAssessment;
    }

    public void setAssessment(int assessment) {
        mAssessment = assessment;
    }

    @Override
    public int compareTo(MeasureWithNameEntity measureWithNameEntity) {
        return this.mMetricName.compareTo(measureWithNameEntity.getMetricName());
    }
}
