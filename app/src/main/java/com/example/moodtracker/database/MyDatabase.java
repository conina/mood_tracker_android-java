package com.example.moodtracker.database;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.moodtracker.Utils.DateConverter;

import java.util.Date;
import java.util.concurrent.Executors;

@Database(entities = {MeasureEntity.class, MetricNameEntity.class}, version = 1)
@TypeConverters({DateConverter.class})
public abstract class MyDatabase extends RoomDatabase {

    private static volatile MyDatabase INSTANCE;


    public static MyDatabase getInstance(final Context context) {
        if (INSTANCE == null) {
            synchronized (MyDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            MyDatabase.class, "my_database")
                            .allowMainThreadQueries()
                            .addCallback(new Callback() {
                        @Override
                        public void onCreate(@NonNull SupportSQLiteDatabase db) {
                            super.onCreate(db);
                            Executors.newSingleThreadScheduledExecutor().execute(new Runnable() {
                                @Override
                                public void run() {
                                    getInstance(context).mMetricNameDao().insert(new MetricNameEntity("Mood"));
                                }
                            });
                        }
                    })
                            .build();

                }
            }
        }
        return INSTANCE;
    }

    public abstract MeasureDao mMeasureDao();

    public abstract MetricNameDao mMetricNameDao();

    }

