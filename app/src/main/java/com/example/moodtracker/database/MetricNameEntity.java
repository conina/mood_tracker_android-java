package com.example.moodtracker.database;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;


//indices are added to enforce the uniqueness of the column metric_name
@Entity (tableName = "metric_names",
indices = {@Index(value = "metric_name", unique = true)})
public class MetricNameEntity {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    @ColumnInfo(name = "metric_name")
    private String mMetricName;

    public MetricNameEntity(String metricName) {
        mMetricName = metricName;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getMetricName() {
        return mMetricName;
    }

    public void setMetricName(String metricName) {
        mMetricName = metricName;
    }


}
