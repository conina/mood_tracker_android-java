package com.example.moodtracker.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "measures_table",
        foreignKeys = @ForeignKey(entity = MetricNameEntity.class,
                parentColumns = "id",
                childColumns = "metric_id",
                onDelete = ForeignKey.CASCADE))
public class MeasureEntity {

    @PrimaryKey (autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    @ColumnInfo(name = "metric_id")
    private int mMetricId;

    @ColumnInfo(name = "date")
    private Date mDate;

    @ColumnInfo(name = "assessment")
    private int mAssessment;

    public MeasureEntity(int metricId, Date date, int assessment) {
        this.mMetricId = metricId;
        this.mAssessment = assessment;
        this.mDate = date;
    }

    public int getMetricId() {
        return mMetricId;
    }

    public void setMetricId(int metricId) {
        mMetricId = metricId;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public int getAssessment() {
        return mAssessment;
    }

    public void setAssessment(int assessment) {
        mAssessment = assessment;
    }

}
