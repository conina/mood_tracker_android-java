package com.example.moodtracker.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface MetricNameDao {

    @Insert
    long insert(MetricNameEntity metricNameEntity);

    @Query("DELETE FROM metric_names")
    void deleteAll();

    @Query("DELETE FROM metric_names WHERE metric_name = :metric")
    void deleteMetricByName(String metric);

    @Query("SELECT * FROM metric_names WHERE metric_name = :metric")
    MetricNameEntity findMetricIdByName(String metric);

    @Query("SELECT * FROM metric_names")
    LiveData<List<MetricNameEntity>> getAllMetricNames();

    @Query("SELECT COUNT(*) FROM metric_names")
    LiveData<Integer>getNumOfMetricNames();

    //only for testing to get an unwrapped list
    @Query("SELECT * FROM metric_names")
    List<MetricNameEntity> getAllMetricsList();

}
