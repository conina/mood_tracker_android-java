package com.example.moodtracker.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.Date;
import java.util.List;

@Dao
public interface MeasureDao {

    @Insert
    void insert(MeasureEntity measureEntity);

    @Query("SELECT * FROM measures_table ORDER BY date")
    LiveData<List<MeasureEntity>> getAllMeasureEntries();

    @Query("DELETE FROM measures_table")
    void deleteAll();

    //only for testing to get an unwrapped list
    @Query("SELECT * FROM measures_table ORDER BY date")
    List<MeasureEntity> getAllMeasureEntriesList();

    @Query("SELECT metric_names.id AS mMetricId, metric_names.metric_name AS mMetricName, measures_table.date AS mDate, measures_table.assessment AS mAssessment " +
            "FROM metric_names " +
            "JOIN measures_table " +
            "ON metric_names.id = measures_table.metric_id ORDER BY date")
    LiveData<List<MeasureWithNameEntity>> getAllMeasuresWithNames();

    @Query("SELECT metric_names.id AS mMetricId, metric_names.metric_name AS mMetricName, measures_table.date AS mDate, measures_table.assessment AS mAssessment  " +
            "FROM metric_names " +
            "JOIN measures_table " +
            "ON metric_names.id = measures_table.metric_id WHERE metric_name = :metric")
    List<MeasureWithNameEntity> getAllMeasuresForThisMetric(String metric);

   /* @Query("SELECT * FROM MeasureEntity WHERE id IN (:ids)")
    List<MeasureDao> loadAllByIds(int[] ids);*/

   @Query("SELECT metric_names.id AS mMetricId, metric_names.metric_name AS mMetricName, measures_table.date AS mDate, measures_table.assessment AS mAssessment  " +
           "FROM metric_names " +
           "JOIN measures_table " +
           "ON metric_names.id = measures_table.metric_id WHERE date = :date")
   LiveData<List<MeasureWithNameEntity>> getAllMeasuresForThisDate(Date date);


}
