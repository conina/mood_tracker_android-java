package com.example.moodtracker.Utils;

import android.content.Context;
import android.graphics.Canvas;
import android.widget.TextView;

import com.example.moodtracker.R;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyMarkerView extends MarkerView {

    private TextView tvContent1;
    private Chart mChart;


    private long mFirstDate;
    public MyMarkerView(Context context, int layoutResource, long firstDate, Chart chart) {
        super(context, layoutResource);
        // find your layout components
        tvContent1 = (TextView) findViewById(R.id.tvContent1);
        mFirstDate = firstDate;
        mChart = chart;
        setChartView(mChart);
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {

        Long longDate = (long) e.getX() + mFirstDate;
        Date realDate = new Date(longDate);
        DateFormat dateFormat1 = new SimpleDateFormat("MMM dd yyyy");
        String stringDate = dateFormat1.format(realDate);
        tvContent1.setText(stringDate + "\nassessment: " + (int) e.getY());


        // this will perform necessary layouting
        super.refreshContent(e, highlight);
    }

    private MPPointF mOffset;
    @Override
    public MPPointF getOffset() {
        if(mOffset == null) {
            // center the marker horizontally and vertically
            mOffset = new MPPointF(-(getWidth() / 2), -getHeight());
        }
        return mOffset;
    }
    @Override
    public void draw(Canvas canvas, float posX, float posY) {
        super.draw(canvas, posX, posY);
        getOffsetForDrawingAtPoint(posX, posY);
    }

}