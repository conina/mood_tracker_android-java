package com.example.moodtracker;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainDisplayNotLoggedTodayFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";

    private String mParam1;

    private OnMainDisplayFragmentInteractionListener mListener;

    public MainDisplayNotLoggedTodayFragment() {
        // Required empty public constructor
    }

    public static MainDisplayNotLoggedTodayFragment newInstance(String param1) {
        MainDisplayNotLoggedTodayFragment fragment = new MainDisplayNotLoggedTodayFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_display_not_logged_today, container, false);

        MaterialButton button = view.findViewById(R.id.button_how_do_you_feel);
        TextView mainTextView = view.findViewById(R.id.main_display_text);
        TextView dateTextView = view.findViewById(R.id.main_display_date);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);

            //check the parameter from main activity - if no - the user hasn't logged the metrics for today and the text view is set accordingly
            if (mParam1.equals("no")) {

                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor edit = sharedPreferences.edit();
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.MILLISECOND, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.HOUR, 0);
                Date currentDate = calendar.getTime();
                DateFormat dateFormat1 = new SimpleDateFormat("MMMM dd, \nYYYY");
                String stringDate = dateFormat1.format(currentDate);
                String displayString = " You haven't logged your mood today.";
                mainTextView.setText(displayString);
                dateTextView.setText(stringDate);
            } else {

                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor edit = sharedPreferences.edit();
                long lastLoggedDate = sharedPreferences.getLong(getString(R.string.last_date_entered), 0);
                Date date = new Date(lastLoggedDate);
                DateFormat dateFormat1 = new SimpleDateFormat("MMMM dd, \nYYYY");
                String stringDate = dateFormat1.format(date);
                String displayString = " Last time you logged your metrics and your mood :";
                mainTextView.setText(displayString);
                dateTextView.setText(stringDate);
            }
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onMainDisplayFragmentInteraction();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMainDisplayFragmentInteractionListener) {
            mListener = (OnMainDisplayFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnViewPagerFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnMainDisplayFragmentInteractionListener {
        void onMainDisplayFragmentInteraction();
    }
}
