package com.example.moodtracker;

import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.moodtracker.MainDisplayLoggedTodayFragment.OnMainDisplayListFragmentInteractionListener;
import com.example.moodtracker.database.MeasureWithNameEntity;

import java.util.List;


public class MainDisplayRecyclerViewAdapter extends RecyclerView.Adapter<MainDisplayRecyclerViewAdapter.ViewHolder> {

    private final List<MeasureWithNameEntity> mValues;
    private final OnMainDisplayListFragmentInteractionListener mListener;

    public MainDisplayRecyclerViewAdapter(List<MeasureWithNameEntity> items, OnMainDisplayListFragmentInteractionListener listener) {
        mValues = items;
//        Log.d("MainDisplay date", mValues.get(0).getDate().toString());
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_main_display_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mMetricName.setText(mValues.get(position).mMetricName);
        holder.mMetricAssessmnet.setText(Integer.toString(mValues.get(position).mAssessment));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.OnMainDisplayListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mMetricName;
        public final TextView mMetricAssessmnet;
        public MeasureWithNameEntity mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mMetricName = (TextView) view.findViewById(R.id.item_name);
            mMetricAssessmnet = (TextView) view.findViewById(R.id.assessment);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mMetricAssessmnet.getText() + "'";
        }
    }
}
