package com.example.moodtracker;

import android.animation.TypeConverter;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.InetAddresses;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.moodtracker.Utils.DateConverter;
import com.example.moodtracker.Utils.MyMarkerView;
import com.example.moodtracker.database.MeasureEntity;
import com.example.moodtracker.database.MeasureWithNameEntity;
import com.example.moodtracker.database.MetricNameEntity;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.IMarker;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.EntryXComparator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


//refactor this in this way - https://stackoverflow.com/questions/52543873/mpandroidchart-multiple-linedataset-from-livedata-in-a-linechart
// stuff.observe(this, goodStuff -> {
//    Data data = generateData(goodStuff);
//    mChart.setData(data);
//    mChart.notifyDataSetChanged(); // let the chart know it's data changed
//    mChart.invalidate(); // refresh chart
//});

public class GraphFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    static List<Date> listOfDates = new ArrayList<>();
    static List<Long> listOfLongsDates = new ArrayList<>();

    ViewModel mViewModel;
    LineChart lineChart;

    List<MeasureEntity> mMeasureEntities;

    List<MetricNameEntity> mMetricNameEntities;

    //graph - local list of all measures with names that's updated when a new measure is entered
    List<MeasureWithNameEntity> mMeasureWithNameEntities;

    //colors of dataset lines
    List<Integer> colors;
    //keep track of colors
    int count;

    //first date to draw on the chart
    Long firstDateToLong;


    //latest date entered
    Long latestDateToLond;


    List<ILineDataSet> dataSetsOfEntryLists;
    LineDataSet dataSet;

    private OnGraphFragmentInteractionListener mListener;
    private List<MetricNameEntity> mAllMetricNames;


    Toast mCurrentToast;

    public GraphFragment() {
    }


    public static GraphFragment newInstance() {
        GraphFragment fragment = new GraphFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //instantiate member lists
        mMeasureWithNameEntities = new ArrayList<>();
        mMetricNameEntities = new ArrayList<>();

        mViewModel = ViewModelProviders.of(getActivity()).get(MyViewModel.class);


        LiveData<List<MetricNameEntity>> metricNamesLiveData = ((MyViewModel) mViewModel).getAllMetricNames();
        LiveData<List<MeasureWithNameEntity>> measuresWithNamesLiveData = ((MyViewModel) mViewModel).getAllMeasuresWithNames();
        ((MyViewModel) mViewModel).getAllMetricNames().observe(this, new Observer<List<MetricNameEntity>>() {
            @Override
            public void onChanged(List<MetricNameEntity> metricNameEntities) {
                //setting the list of names so that Fragment can pull the name for the UI
                mMetricNameEntities = metricNameEntities;
                //setting the number of fragments so that adapter can create new fragment for the added metric
            }
        });

        setUpColorsArray();


    }

    private void setUpColorsArray() {
        colors = new ArrayList<>();
        int color0 = getResources().getColor(R.color.primary);
        int color1 = getResources().getColor(R.color.color_1);
        int color2 = getResources().getColor(R.color.color_2);
        int color3 = getResources().getColor(R.color.color_3);
        int color4 = getResources().getColor(R.color.color_4);
        int color5 = getResources().getColor(R.color.color_5);
        int color6 = getResources().getColor(R.color.color_6);
        int color7 = getResources().getColor(R.color.color_7);
        int color8 = getResources().getColor(R.color.color_8);
        int color9 = getResources().getColor(R.color.color_9);
        int color10 = getResources().getColor(R.color.color_10);

        List<Integer> color_list = Arrays.asList(color0, color1, color2, color3, color4, color5, color6, color7, color8, color9, color10);
        colors.addAll(color_list);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_graph, container, false);

        //setting up the LineChart
        lineChart = view.findViewById(R.id.chart);

        latestDateToLond = (long) 0;


        count = 0;

        //observe the table with all measures
        mViewModel = ViewModelProviders.of(getActivity()).get(MyViewModel.class);
        ((MyViewModel) mViewModel).getAllMeasuresWithNames().observe(this, new Observer<List<MeasureWithNameEntity>>() {
            @Override
            public void onChanged(List<MeasureWithNameEntity> measureWithNameEntities) {

                //if there is nothing in the database, return
                if (measureWithNameEntities.size() == 0) {
                    return;
                }

                //get the list of all metric names
//                mMetricNameEntities = mAllMetricNames;
                firstDateToLong = measureWithNameEntities.get(0).getDate().getTime();

                // instantiating a list of data sets that will be drawn to the chart
                // one metric name with all measures is one data set
                //data sets will be added one by one
                dataSetsOfEntryLists = new ArrayList<>();

                //for each metric name in the metric list, find all measures in the measuresWithNames list,
                // create a data set with all measures for that metric and add it to the list of data sets that will be drawn on the graph
                for (MetricNameEntity metricNameEntity : mMetricNameEntities) {

                    //instantiating a list of entry objects for One Metric
                    List<Entry> entryListForOneMetric = new ArrayList<>();

                    //get all measures for one metric form the database
                    List<MeasureWithNameEntity> measuresForOneMetric = ((MyViewModel) mViewModel).getMeasuresForMetricWithName(metricNameEntity.getMetricName());

                    //name of the metric that we're currently looping through
                    String currentMetricName = metricNameEntity.getMetricName();

                    //cycle through all measures for that metric; get the assessment and the date and put it in the entryListForOneMetric
                    for (int i = 0; i < measuresForOneMetric.size(); i++) {

                        MeasureWithNameEntity oneMeasure = measuresForOneMetric.get(i);

                        //date for one measure - to be shown on labels
                        Date dateOneMeasure = oneMeasure.getDate();

                        Log.d(oneMeasure.getMetricName() + i, Integer.toString(oneMeasure.getAssessment()));

                        //convert it to long and subtract the first date so the value of the date is drawn on the graph correctly
                        Long dateOneMeasureLong = (dateOneMeasure.getTime() - firstDateToLong);

                        //set the latest date entered
                        if (dateOneMeasureLong > latestDateToLond) {
                            latestDateToLond = dateOneMeasureLong;
                        }

                        //assessment for one measure
                        int assessmentOneMeasure = oneMeasure.getAssessment();

                        //entry object for one measure for one metric
                        Entry oneMeasureEntry = new Entry(dateOneMeasureLong, assessmentOneMeasure);

                        entryListForOneMetric.add(oneMeasureEntry);

                        //sort in ascending order
                        entryListForOneMetric.sort(new EntryXComparator());


                    }

                    //creating new data set with the metric name and all measures for that metric name
                    dataSet = new LineDataSet(entryListForOneMetric, currentMetricName);

                    //formatting the values of data points that are shown from float to integer
                    dataSet.setValueFormatter(new ValueFormatter() {
                        @Override
                        public String getFormattedValue(float value) {
                            return String.valueOf((int) value);
                        }
                    });

                    //check if the metric name is the Mood question in which case the line should be a bit ticker
                    dataSet.setValueTextSize(15);
                    if (currentMetricName.equals("Mood")) {
                        dataSet.setLineWidth(7);
//                        dataSet.setCircleRadius(0);
                        dataSet.setDrawCircles(false);

                    } else {
                        dataSet.setLineWidth(3);
                        dataSet.setCircleRadius(5);
                        dataSet.setDrawCircles(true);


                    }

                    dataSet.setDrawValues(false);
                    int dataColor = setDataSetColor(dataSet);
                    dataSet.setCircleColor(dataColor);
                    dataSet.setCircleHoleColor(dataColor);


                    Log.d("metric name", currentMetricName);
                    Log.d("size of entry list for " + currentMetricName, Integer.toString(entryListForOneMetric.size()));

                    //adding the current data set (all measures for one metric) to the list of data sets that will be drawn
                    dataSetsOfEntryLists.add(dataSet);
                    Log.d("Number of data sets", Integer.toString(dataSetsOfEntryLists.size()));
                }

                drawChart();

            }
        });

        return view;
    }

    private void drawChart() {
        //setting up the chart
        LineData lineData = new LineData(dataSetsOfEntryLists);
        lineChart.setData(lineData);
        lineChart.invalidate(); // refresh

        lineChart.setExtraBottomOffset(35);
        lineChart.setExtraTopOffset(135);
        lineChart.setTouchEnabled(true);
        lineChart.setPinchZoom(true);
        lineChart.getDescription().setEnabled(false);


        //setting up a markervie so that a popup window is shown when an Entry point is clicked
        IMarker marker = new MyMarkerView(getActivity(), R.layout.custem_marker_view_layout, firstDateToLong, lineChart);
        lineChart.setMarker(marker);

        //formatting X axis
        XAxis xAxis = lineChart.getXAxis();

        //to enforce showing only ten days on screen
//        float minXRange = 864000000;
//        float maxXRange = 864000000; //10 days
//        lineChart.setVisibleXRange(minXRange, maxXRange);
//        xAxis.setLabelCount(10, true);

        //formatting the dates that will appear on the X axis so that they start from the
        //first logged date and not from the beginning of Java time (January 1, 1970)
        formatXAxisDates(xAxis);

        xAxis.setGranularityEnabled(true);
        xAxis.setGranularity(86400000);

//        xAxis.setCenterAxisLabels(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxis.setLabelRotationAngle(35);
//        xAxis.setAvoidFirstLastClipping(true);

        //USE THIS TO SET THE GRAPH VIEWS FOR ONE MONTH/6MONTHS/YEAR
//        xAxis.setLabelCount(5, true);
//        xAxis.setAxisMaximum(86400000 * 3);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setTextSize(8);
        xAxis.setDrawGridLines(false);
        xAxis.setTypeface(getResources().getFont(R.font.primary_font));
        xAxis.setTextColor(getResources().getColor(R.color.primary_text));

        //formatting left and right Y axis
        final YAxis axisLeft = lineChart.getAxisLeft();

        axisLeft.setDrawLabels(true);
        axisLeft.setAxisMinimum(1);
        axisLeft.setAxisMaximum(6);
        axisLeft.setLabelCount(6, true);
        axisLeft.setDrawTopYLabelEntry(false);
        axisLeft.setDrawGridLines(false);
        axisLeft.setTypeface(getResources().getFont(R.font.primary_font));
        axisLeft.setTextColor(getResources().getColor(R.color.primary_text));
        axisLeft.setTextSize(12);

        final YAxis axisRight = lineChart.getAxisRight();
        axisRight.setDrawLabels(false);
        axisRight.setDrawGridLines(false);

        //formatting legend
        Legend legend = lineChart.getLegend();
        legend.setDrawInside(false);
        legend.setTextColor(getResources().getColor(R.color.primary_text));
//        legend.setTypeface(getResources().getFont(R.font.primary_font));

//        legend.setTextSize(14);
        legend.setWordWrapEnabled(true);
        legend.setForm(Legend.LegendForm.CIRCLE);
        legend.setXEntrySpace(20f);
        legend.setFormToTextSpace(5f);
        legend.setYEntrySpace(5f);
        legend.setYOffset(20);

    }


    private void formatXAxisDates(XAxis xAxis) {
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                Long dateLong = (long) value + firstDateToLong;
                Log.d("Long date: ", String.valueOf(dateLong));
                Date date = new Date(dateLong);
                DateFormat dateFormat1 = new SimpleDateFormat("MMM dd");
                String stringDate = dateFormat1.format(date);

                return stringDate;
            }

        });
    }

    private int setDataSetColor(DataSet dataSet) {


        int color = colors.get(count);
        dataSet.setColor(color, 150);
        //increase the count of colored datasets so that the next dataset gets the next color
        count = count + 1;
        return color;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnGraphFragmentInteractionListener) {
            mListener = (OnGraphFragmentInteractionListener) context;

        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnGraphFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnGraphFragmentInteractionListener {

        void onGraphFragmentInteraction(Uri uri);
    }


}

