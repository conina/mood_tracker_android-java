package com.example.moodtracker;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.moodtracker.database.MeasureWithNameEntity;
import com.google.android.material.button.MaterialButton;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class MainDisplayLoggedTodayFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 2;
    private OnMainDisplayListFragmentInteractionListener mListener;
    MaterialButton changeRatings;
    TextView metricsForToday;
    TextView mainDispalyListDate;
    private List<MeasureWithNameEntity> mMeasuresForToday;

    public MainDisplayLoggedTodayFragment() {
    }

    @SuppressWarnings("unused")
    public static MainDisplayLoggedTodayFragment newInstance(int columnCount) {
        MainDisplayLoggedTodayFragment fragment = new MainDisplayLoggedTodayFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //instantiate the list
        mMeasuresForToday = new ArrayList<>();
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_display_item_list, container, false);


        //find textview and the button
        metricsForToday = view.findViewById(R.id.metrics_for_today);
        changeRatings = view.findViewById(R.id.change_ratings);
        mainDispalyListDate = view.findViewById((R.id.date_for_main_display_list));
        addListenerOnChangeRatingsButton();

        metricsForToday.setText("Your ratings for ");
        // Set the adapter
        Context context = view.getContext();
        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.main_display_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }

        //getting metrics for today from the viewmodel
        final ViewModel mViewModel = ViewModelProviders.of(getActivity()).get(MyViewModel.class);

        //today' date
            Date today_date;
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            today_date = calendar.getTime();
            Log.d("Today's date", today_date.toString());

        ((MyViewModel) mViewModel).getPickedDate().observe(this, new Observer<Date>() {
            @Override
            public void onChanged(Date date) {

                //set up the date textfield
                Long pickedDateLong = date.getTime();
                DateFormat dateFormat1 = new SimpleDateFormat("MMMM dd, YYYY");
                final String stringDate = dateFormat1.format(pickedDateLong);
                mainDispalyListDate.setText(stringDate);

                ((MyViewModel) mViewModel).getMeasuresForThisDate(date).observe(getViewLifecycleOwner(), new Observer<List<MeasureWithNameEntity>>() {
                    @Override
                    public void onChanged(List<MeasureWithNameEntity> measureWithNameEntities) {
//                        Log.d("First item MainDisplay", measureWithNameEntities.get(0).getMetricName());

                        if (measureWithNameEntities.isEmpty()) {
                            Toast.makeText(getActivity(), "No ratings for " + stringDate, Toast.LENGTH_SHORT).show();
                        }
                        mMeasuresForToday = measureWithNameEntities;
                        recyclerView.setAdapter(new MainDisplayRecyclerViewAdapter(mMeasuresForToday, mListener));
                    }
                });

            }
        });


        return view;
    }

    private void addListenerOnChangeRatingsButton() {
        changeRatings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.createViewPagerForThisDate();
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMainDisplayListFragmentInteractionListener) {
            mListener = (OnMainDisplayListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnMainDisplayListFragmentInteractionListener {
        // TODO: Update argument type and name
        void OnMainDisplayListFragmentInteraction(MeasureWithNameEntity item);

        void createViewPagerForThisDate();
    }
}
