package com.example.moodtracker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;
import androidx.preference.PreferenceManager;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.DatePicker;

import com.example.moodtracker.database.MeasureWithNameEntity;
import com.example.moodtracker.database.MetricNameEntity;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


//TODO

//edditing data should be the same as putting new data in - first check if there is data
//FIGURE OUT HOW TO GET MEASURES FROM TODAY ONLY!!!!!!! now it's set in MeasureDao to get all dates that are bigger or smaller than today's date - for some reason time
// for dates are different even though calendar hour, min, sec set to zero  - Calendar HOUR OF DAY different from HOUR


//add possibility to choose your scale
//CLEAN MAIN ACTIVITY
//FIX THE BUG when first open graph fragment before entering new data - probably something wiht LiveData now that we changed the way fragments are added/removed
//I figured why - we are pulling the list from template fragment (TemplateFragment.sListOfMetricNames) which is not created yet now with this new
// organization so - we need two sources form DB (for names and for mesures) maybe use MediatorLiveData or save local list with names
// - https://medium.com/@elye.project/understanding-live-data-made-simple-a820fcd7b4d0
// we need to create the list in the graph fragment
//figure out the best way to manage fragments from main activity SOLVED
// back arrow - set the main display as default fragment but if you press back arrow from this fragment exit app SOLVED
//interaction with chart - create MarkerView SOLVED
//create new instances instead of new GraphFragment SOLVED
//in fragment AddMetric - change the logic to compare with the list from viewmodel and not from TemplateFragment static list
//move the addMetric fragment to be called from here and not ItemFragment
//make How do you feel today? dataset distinctive on the graph
//figure out how to tell adapter or recylerview to delete the position od the item when deleted SOLVED
//add a functionality to delete a metric when loong click - add three dots besides every metric name and show a little pop-up with the confirmation
//set up graph with multiple views - monthlu, yearly
//USE something like THIS TO SET THE GRAPH VIEWS FOR ONE MONTH/6MONTHS/YEAR
//        xAxis.setLabelCount(5, true);
//        xAxis.setAxisMaximum(864000000);


public class MainActivity extends AppCompatActivity implements AddMetricFragment.AddMetricListener,
        TemplateFragment.AddMeasureListener, GraphFragment.OnGraphFragmentInteractionListener,
        ItemFragment.OnListFragmentInteractionListener, ViewPagerFragment.OnViewPagerFragmentInteractionListener, MainDisplayNotLoggedTodayFragment.OnMainDisplayFragmentInteractionListener , MainDisplayLoggedTodayFragment.OnMainDisplayListFragmentInteractionListener{

    MyViewModel mMyViewModel;
    static Date sPickedDate;
    private long mLastLoggedDate;
    private Date mCurrentDate;
    public int radio_button_clicked = R.integer.nothing_selected;
    private Map<String, Integer> temp_metric_assessments;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        //setup the toolbar with a back arrow
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //temporary map of metrics and ratings for one date
        temp_metric_assessments = new HashMap<>();

        //create an instance of MyViewModel
        mMyViewModel = ViewModelProviders.of(this).get(MyViewModel.class);

        //set up current date
        mCurrentDate = setUpCurrentDate();

        //put main fragment in the container
        putMainDisplayFragment();

        //show AddMetricFragment if the list of metrics is empty
        if (checkIfDatabaseEmpty()) {
            putFragmentInContainer(new AddMetricFragment());
        }


    }

    private Date setUpCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        Date currentDate = calendar.getTime();
        return currentDate;
    }

    //show a different MainDisplayNotLoggedTodayFragment depending on whether metrics were logged today or not
    private void putMainDisplayFragment() {
        if (metricsLoggedToday()) {
            putFragmentInContainer(MainDisplayLoggedTodayFragment.newInstance(1));
        } else {
            putFragmentInContainer(MainDisplayNotLoggedTodayFragment.newInstance("yes"));


        }
    }

    private boolean metricsLoggedToday() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        mLastLoggedDate = sharedPreferences.getLong(getString(R.string.last_date_entered), 0);

//        Date forTestingDate = new Date(mLastLoggedDate);
//        Log.d("Last logged date:", Long.toString(mLastLoggedDate) );
//        Log.d("Current date:", Long.toString(mCurrentDate.getTime()) );

        return (mCurrentDate.getTime() == mLastLoggedDate);
    }

    private boolean checkIfDatabaseEmpty() {
        return (mMyViewModel.getAllMetricsList().size() <= 1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.graph) {
            GraphFragment graphFragment = GraphFragment.newInstance();
            putFragmentInContainer(graphFragment);
        }
        if (id == R.id.set_new_date) {
            DialogFragment newDateFragment = new DatePickerFragment();
            newDateFragment.show(getSupportFragmentManager(), "datePicker");
        }
        if (item.getItemId() == R.id.list_metrics) {
            ItemFragment itemFragment = ItemFragment.newInstance(getResources().getInteger(R.integer.number_of_columns));
            putFragmentInContainer(itemFragment);
        }
        if (item.getItemId() == android.R.id.home) {
            FragmentManager fm = getSupportFragmentManager();
            Fragment fragment = fm.findFragmentById(R.id.main_activity_fragment_container);
            if (fragment != null) {
                if ((fragment instanceof MainDisplayNotLoggedTodayFragment) || (fragment instanceof MainDisplayLoggedTodayFragment))  {
                    finish();
                } else {
                    putMainDisplayFragment();
//                    FragmentTransaction ft = fm.beginTransaction();
////                    ft.remove(fragment);
//                    putFragmentInContainer(MainDisplayNotLoggedTodayFragment.newInstance("yes"));
//                    ft.commit();
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void putFragmentInContainer(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.replace(R.id.main_activity_fragment_container, fragment);
        ft.commit();
    }

    @Override
    public void addMetricToDatabase(MetricNameEntity metricNameEntity) {
        mMyViewModel.insertMetricNameIntoDatabase(metricNameEntity);
        putFragmentInContainer(MainDisplayNotLoggedTodayFragment.newInstance("yes"));
    }

    @Override
    public void putMetricListFragmentIntoContainer(Fragment addMetricFragment) {
        //remove the fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(addMetricFragment);
        fragmentTransaction.commit();
        putFragmentInContainer(ItemFragment.newInstance(getResources().getInteger(R.integer.number_of_columns)));


    }

    @Override
    public void put_to_temp_map(String metricName, int radioButton) {
        temp_metric_assessments.put(metricName, radioButton);
    }

    @Override
    public void addMeasuresToDatabase() {
        if (sPickedDate == null) {
            sPickedDate = mCurrentDate;
        }

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor edit = sharedPreferences.edit();
        mLastLoggedDate = sharedPreferences.getLong(getString(R.string.last_date_entered), 0);

        if (sPickedDate.getTime() > mLastLoggedDate) {
            edit.putLong(getString(R.string.last_date_entered), sPickedDate.getTime());
            edit.commit();
        }


        for (Map.Entry me : temp_metric_assessments.entrySet()) {
            System.out.println("Key: "+me.getKey() + " & Value: " + me.getValue());
            String metricName = (String) me.getKey();
            Integer radioButton = (int) me.getValue();


            mMyViewModel.insertMeasureIntoDatabase(metricName, sPickedDate, radioButton);

        }
    }

    //when interacting with fragments
    @Override
    public void onGraphFragmentInteraction(Uri uri) {
    }

    @Override
    public void onListFragmentInteraction(MetricNameEntity item) {
        mMyViewModel.deleteMetricByName(item.getMetricName());
    }

    @Override
    public void showAddMetricFragment() {
        AddMetricFragment addMetricFragment = new AddMetricFragment();
        addMetricFragment.show(getSupportFragmentManager(), "Add Metric");
    }

    @Override
    public void onViewPagerFragmentInteraction(Uri uri) {
    }

    @Override
    public void onMainDisplayFragmentInteraction() {
        putFragmentInContainer(ViewPagerFragment.newInstance());
    }

    @Override
    public void OnMainDisplayListFragmentInteraction(MeasureWithNameEntity item) {

    }

    @Override
    public void createViewPagerForThisDate() {
        putFragmentInContainer(ViewPagerFragment.newInstance());

    }


    // Date Fragment
    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            c.set(Calendar.MILLISECOND, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.HOUR_OF_DAY, 0);

            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            Dialog dateDialog = new DatePickerDialog(getActivity(), this, year, month, day);
            ((DatePickerDialog) dateDialog).getDatePicker().setMaxDate(System.currentTimeMillis());
            // Create a new instance of DatePickerDialog and return it
            return dateDialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Set the Date Object to insert into the database
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.HOUR_OF_DAY, 0);

            sPickedDate = calendar.getTime();
            ViewModel mMyViewModel = ViewModelProviders.of(getActivity()).get(MyViewModel.class);
((MyViewModel) mMyViewModel).setPickedDate(sPickedDate);

        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }
}


