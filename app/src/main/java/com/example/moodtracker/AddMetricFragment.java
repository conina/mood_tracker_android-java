package com.example.moodtracker;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.moodtracker.database.MetricNameEntity;
import com.google.android.material.button.MaterialButton;

public class AddMetricFragment extends DialogFragment {

    MaterialButton mAddButton;
    EditText mNewMetric;

    private AddMetricListener mAddMetricListener;

    public AddMetricFragment() {
/*
        setRetainInstance(true);
*/

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void onAttach(Context context) {
        super.onAttach(context);

        // setting the MainActivity as a listener so that new data can be saved to the database
        try {
            mAddMetricListener = (AddMetricListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement " + AddMetricListener.class.getName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mAddMetricListener = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
//        // Get existing layout params for the window
//        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
//        // Assign window properties to fill the parent
//        params.width = WindowManager.LayoutParams.MATCH_PARENT;
//        params.height = WindowManager.LayoutParams.MATCH_PARENT;
//        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
//        // Call super onResume after sizing

        super.onResume();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.add_metric_fragment, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAddButton = getView().findViewById(R.id.add_button);
        mNewMetric = getView().findViewById(R.id.new_metric);

        //add listener to the add button
        addListenerOnAddButton();
    }

    public void addListenerOnAddButton() {


        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String strNewMetric = mNewMetric.getText().toString();
                Log.d("Entered METRIC", strNewMetric + "    LALALALLAL");

                if (TextUtils.isEmpty(strNewMetric)) {
                    Toast.makeText(getActivity(), "Enter the fucking metric!", Toast.LENGTH_SHORT).show();
                } else {
                    if (mAddMetricListener != null) {

                        //checking if the metric name already exists
                        for (int i = 0; i < TemplateFragment.sListOfMetricNames.size(); i++) {
                            Log.d("Metric name:", TemplateFragment.sListOfMetricNames.get(i).getMetricName());
                            if (TemplateFragment.sListOfMetricNames.get(i).getMetricName().equals(strNewMetric)) {
                                Toast.makeText(getActivity(), strNewMetric + " already exists. Try again.", Toast.LENGTH_SHORT).show();
                                return;

                            }
                        }

                        //add to database
                        mAddMetricListener.addMetricToDatabase(new MetricNameEntity(strNewMetric));


                        mAddMetricListener.putMetricListFragmentIntoContainer(AddMetricFragment.this);


                    }

                }

            }
        });
    }

    public interface AddMetricListener {
        void addMetricToDatabase(MetricNameEntity metricNameEntity);
        void putMetricListFragmentIntoContainer(Fragment fragment);

    }




}
